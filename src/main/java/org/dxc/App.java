package org.dxc;

import java.util.Iterator;
import java.util.List;

import org.dxc.entity.Employee;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App {
	private static SessionFactory factory;

	public static void main(String[] args) {
		try {
			factory = new Configuration().configure("resource/hibernate.cfg.xml").buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session Instance");
			throw new ExceptionInInitializerError(e);
		}

		App employe = new App();

// //   /*Add Employee Details*/
//   employe.addEmployee("Nuthan", "Preeth", 1000);
//   employe.addEmployee("Rudresh", "Aradya", 5000);
//  employe.addEmployee("Akhil", "Banagiri", 3000);

		// List Employees
    employe.listEmployees();
		
		//Delete Employee
		//employe.deleteEmployee(1);
		
		//Update Employee
		//employe.updateEmployee(3, 1);
		
//    employe.getEmployeeById(1);
//    
	}

	public void getEmployeeById(int id) {
		Transaction tx=null;
		Session session = factory.openSession();
		
		try {
			tx=session.beginTransaction();
			Employee employee = (Employee) session.get(Employee.class, id);
			System.out.print("First Name:" + employee.getFirstName());
			System.out.print("Last Name:" + employee.getLastName());
			System.out.println("  Salary:" + employee.getSalary());
			
		}catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} catch (NullPointerException e) {
			System.out.println( "id: "+ id + " Not Found");
		}
		finally {
			session.close();
		}
		
	}
	
	public Integer addEmployee(String fname, String lname, int salary) {
		// Session object to be extracted from factory
		Session session = factory.openSession();
		Transaction tx = null;
		Integer employeeID = null;

		try {
			tx = session.beginTransaction();
			Employee employee = new Employee(fname, lname, salary);
			// add it to relational model
			employeeID = (Integer) session.save(employee);
			tx.commit();
			System.out.println("Employee created");
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} 
		finally {
			session.close();
		}

		return employeeID;

	}

	/* Method to READ all the employees */
	public void listEmployees() {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			List employees = session.createQuery("FROM Employee").list();
			for (Iterator iterator = employees.iterator(); iterator.hasNext();) {
				Employee employee = (Employee) iterator.next();
				System.out.print("First Name: " + employee.getFirstName());
				System.out.print("  Last Name: " + employee.getLastName());
				System.out.println("  Salary: " + employee.getSalary());
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/* Update salary for an employee */
	public void updateEmployee(Integer EmployeeID, int salary) {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Employee employee = (Employee) session.get(Employee.class, EmployeeID);
			employee.setSalary(salary);
			session.update(employee);
			tx.commit();
			System.out.println("Successfully Updated!");
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} catch(IllegalArgumentException e) {
			System.out.println("EmployeeID does not exist!");
		}  
		finally {
			session.close();
		}
	}

	/* Delete an employee from the records */
	public void deleteEmployee(Integer EmployeeID) {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Employee employee = (Employee) session.get(Employee.class, EmployeeID);
			session.delete(employee);
			tx.commit();
			System.out.println("EmployeeID "+EmployeeID + " successfully deleted!");
		} catch (HibernateException e) {

			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();

		}catch(IllegalArgumentException e) {
			System.out.println("EmployeeID does not exist!");
		} 
		finally {
			session.close();
		}
	}

}
